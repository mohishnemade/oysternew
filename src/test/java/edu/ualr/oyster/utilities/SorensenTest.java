package edu.ualr.oyster.utilities;

import org.junit.Test;

public class SorensenTest {

	@Test
	public void testComputeDistance() {
        String s, t;
        Sorensen j = new Sorensen();
        s = "CRUSAN"; t = "CHRZAN";
        System.out.format("%1$,6.5f %2$s %3$s%n", j.computeDistance(s, t), s, t);
        
        s = "ISLE"; t = "ISELEY";
        System.out.format("%1$,6.5f %2$s %3$s%n", j.computeDistance(s, t), s, t);
        
        s = "PENNING"; t = "PENINGTON";
        System.out.format("%1$,6.5f %2$s %3$s%n", j.computeDistance(s, t), s, t);
        
        s = "PENNINGTON"; t = "PENINGTON";
        System.out.format("%1$,6.5f %2$s %3$s%n", j.computeDistance(s, t), s, t);
        
        s = "STROHMAN"; t = "STROHM";
        System.out.format("%1$,6.5f %2$s %3$s%n", j.computeDistance(s, t), s, t);
        
        s = "EDUARDO"; t = "EDWARD";
        System.out.format("%1$,6.5f %2$s %3$s%n", j.computeDistance(s, t), s, t);
        
        s = "JOHNSON"; t = "JAMISON";
        System.out.format("%1$,6.5f %2$s %3$s%n", j.computeDistance(s, t), s, t);
        
        s = "RAPHAEL"; t = "RAFAEL";
        System.out.format("%1$,6.5f %2$s %3$s%n", j.computeDistance(s, t), s, t);
        
        s = "Eric"; t = null;
        System.out.format("%1$,6.5f %2$s %3$s%n", j.computeDistance(s, t), s, t);
	}

}
