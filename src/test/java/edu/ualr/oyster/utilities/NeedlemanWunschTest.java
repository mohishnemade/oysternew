package edu.ualr.oyster.utilities;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

/**
 * TODO Extend this test.  It does not provide adequate coverage
 * 
 */

public class NeedlemanWunschTest {

	@Test
	public void testNeedlemanWunsch() {
        int match = 10, mismatch = -1, gap = -5;
        NeedlemanWunsch sw = new NeedlemanWunsch();
        // Sequence 1 = A-CACACTA
        // Sequence 2 = AGCACAC-A
        
        float score = sw.computeNeedlemanWunsch("AGCT", "AGCT", match, mismatch, gap);
        System.out.println("Score  : " + score);
        assertEquals(0.0f,score,0.0f);

        score = sw.computeNormalizedScore(match);
        System.out.println("N-Score: " + score);
        assertEquals(0.0f,score,0.0f);
	}
	
	@Ignore
	public void testComputeNeedlemanWunsch() {
		
		fail("Not yet implemented");
	}

	@Ignore
	public void testComputeNormalizedScore() {
		fail("Not yet implemented");
	}

	@Ignore
	public void testComputeNormalizedScore2() {
		fail("Not yet implemented");
	}

}
