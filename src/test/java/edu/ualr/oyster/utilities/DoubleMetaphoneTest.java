package edu.ualr.oyster.utilities;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DoubleMetaphoneTest {
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	/*
	 * Constructor has no functionality so is not tested
	 */
	
	/*
	 * Since all the tests follow the same pattern
	 * The funcionality is encapsulated in this test() method
	 * Failed tests will show this and the calling method in the
	 * stack trace
	 * @Param String term = string to be evaluated
	 * @Param String... = array of expected results, for this there shoudl be two.
	 */
	private String [] test(String term, String... expected) {
		DoubleMetaphone dm = new DoubleMetaphone();
		String [] hash = dm.getDoubleMetaphone(term);
		logger.debug("Term [{}] Results [{}]",term,hash);
		for (int i = 0; i < expected.length; i++) {
			assertEquals(expected[i],hash[i]);
		}
		return hash;
	}

	@Test
	public void test1() {
		// "Nelson" - "NLSN" and "NLSN"
		test("Nelson","NLSN","NLSN");
		// "Neilson" - "NLSN" and "NLSN"
		test("Neilsen","NLSN","NLSN");
	}
	
	@Test
	public void test2() {
		// "Occasionally" - "AKSN" and "AKXN"
		test("Occasionally","AKSN","AKXN");
	}
	
	@Test
	public void test3() {
		// "antidisestablishmentarianism" - "ANTT"
		test("antidisestablishmentarianism","ANTT","ANTT");
	}
	
	@Test
	public void test4() {
		// "appreciated" - "APRS" and "APRX"
		test("appreciated","APRS","APRX");
	}
	
	@Test
	public void test6() {
		// "beginning" - "PJNN" and "PKNN"
		test("beginning","PJNN","PKNN");
	}
	
	@Test
	public void test7() {
		// "changing" - "XNJN" and "XNKN"
		test("changing","XNJN","XNKN");
	}

	@Test
	public void test9() {
		// "cheat" - "XT"
		test("cheat","XT","XT");
	}

	@Test
	public void test10() {
		// "dangerous" - "TNKR" and "TNJR"
		test("dangerous","TNKR","TNJR");
	}

	@Test
	public void test11() {
		// "development" - "TFLP"
		test("development","TFLP","TFLP");
	}

	@Test
	public void test12() {
		// "etiology" - "ATLK" and "ATLJ"
		test("etiology","ATLK","ATLJ");
	}

	@Test
	public void test13() {
		// "existence" - "AKSS"
		test("existence","AKSS","AKSS");
	}

	@Test
	public void test14() {
		// "simplicity" - "SMPL"
		test("simplicity","SMPL","SMPL");
	}

	@Test
	public void test15() {
		// "circumstances" - "SRKM"
		test("circumstances","SRKM","SRKM");
	}

	@Test
	public void test16() {
		// "fiery" - "FR"
		test("fiery","FR","FR");
	}

	@Test
	public void test17() {
		// "february" - "FPRR"
		test("february","FPRR","FPRR");
	}

	@Test
	public void test18() {
		// "illegitimate" - "ALJT" and "ALKT"
		test("illegitimate","ALJT","ALKT");
	}

	@Test
	public void test19() {
		// "immediately" - "AMTT"
		// TODO this got a different result (AMMT instead of AMTT) 
		test("immediately","AMMT","AMMT");
	}

	@Test
	public void tes20() {
		// "happily" - "HPL"
		test("happily","HPL","HPL");
	}

	@Test
	public void test21() {
		// "judgment" - "JTKM" and "ATKM"
		test("judgment","JTKM","ATKM");
	}

	@Test
	public void test22() {
		// "knowing" - "NNK"
		test("knowing","NNK","NNK");
	}

	@Test
	public void test23() {
		// "kipper" - "KPR"
		test("kipper","KPR","KPR");
	}

	@Test
	public void test24() {
		// "john" - "JN" and "AN"
		test("john","JN","AN");
	}

	@Test
	public void test25() {
		// "lesion" - "LSN" and "LXN"
		test("lesion","LSN","LXN");
	}

	@Test
	public void test26() {
		// "Xavier" - "SF" and "SFR"
		test("Xavier","SF","SFR");
	}

	@Test
	public void test27() {
		// "dumb" - "TM"
		test("dumb","TM","TM");
	}

	@Test
	public void test28() {
		// "caesar" - "SSR"
		test("caesar","SSR","SSR");
	}

	@Test
	public void test29() {
		// "chianti" - "KNT"
		test("chianti","KNT","KNT");
	}

	@Test
	public void test30() {
		// "michael" - "MKL" and "MXL"
		test("michael","MKL","MXL");
	}

	@Test
	public void test31() {
		// "chemistry" - "KMST"
		test("chemistry","KMST","KMST");
	}

	@Test
	public void test32() {
		// "chorus" - "KRS"
		test("chorus","KRS","KRS");
	}

	@Test
	public void test33() {
		// "architect - "ARKT"
		test("architect","ARKT","ARKT");
	}

	@Test
	public void test34() {
		// "arch" - "ARX" and "ARK"
		test("arch","ARX","ARK");
	}

	@Test
	public void test35() {
		// "orchestra" - "ARKS"
		test("orchestra","ARKS","ARKS");
	}

	@Test
	public void test36() {
		// "orchid" -  "ARKT"
		test("orchid","ARKT","ARKT");
	}

	@Test
	public void test37() {
		// "wachtler" - "AKTL" and "FKTL"
		test("wachtler","AKTL","FKTL");
	}

	@Test
	public void test38() {
		// "wechsler" - "AKSL" and "FKSL"
		test("wechsler","AKSL","FKSL");
	}

	@Test
	public void test39() {
		// "tichner" - "TXNR" and "TKNR"
		test("tichner","TXNR","TKNR");
	}

	@Test
	public void test40() {
		// "McHugh" - "MK"
		test("McHugh","MK","MK");
	}

	@Test
	public void test41() {
		// "czerny" - "SRN" and "XRN"
		test("czerny","SRN","XRN");
	}

	@Test
	public void test42() {
		// "focaccia" - "FKX"
		test("focaccia","FKX","FKX");
	}

	@Test
	public void test43() {
		// "bellocchio" - "PLX"
		test("bellocchio","PLX","PLX");
	}

	@Test
	public void test44() {
		// "bacchus" - "PKS"
		test("bacchus","PKS","PKS");
	}

	@Test
	public void test45() {
		// "accident" - "AKST"
		test("accident","AKST","AKST");
	}

	@Test
	public void test46() {
		// "accede" - "AKST"
		test("accede","AKST","AKST");
	}

	@Test
	public void test47() {
		// "succeed" - "SKST"
		test("succeed","SKST","SKST");
	}

	@Test
	public void test48() {
		// "bacci" - "PX"
		test("bacci","PX","PX");
	}

	@Test
	public void test49() {
		// "bertucci" - "PRTX"
		test("bertucci","PRTX","PRTX");
	}

	@Test
	public void test50() {
		// "mac caffrey" - "MKFR"
		test("mac caffrey","MKFR","MKFR");
	}

	@Test
	public void test51() {
		// "mac gregor" - "MKRK"
		test("mac gregor","MKRK","MKRK");
	}

	@Test
	public void test52() {
		// "edge" - "AJ"
		test("edge","AJ","AJ");
	}

	@Test
	public void test53() {
		// "edgar" - "ATKR"
		test("edgar","ATKR","ATKR");
	}

	@Test
	public void test54() {
		// "ghislane" - "JLN"
		test("ghislane","JLN","JLN");
	}

	@Test
	public void test55() {
		// ghiradelli - "JRTL"
		test("ghiradelli","JRTL","JRTL");
	}

	@Test
	public void test56() {
		// "hugh" - "H"
		test("hugh","H","H");
	}

	@Test
	public void test57() {
		// "bough" - "P"
		test("bough","P","P");
	}

	@Test
	public void test58() {
		// "broughton" - "PRTN"
		test("broughton","PRTN","PRTN");
	}

	@Test
	public void test59() {
		// "laugh" - "LF"
		test("laugh","LF","LF");
	}

	@Test
	public void test60() {
		// "McLaughlin" - "MKLF"
		test("McLaughlin","MKLF","MKLF");
	}

	@Test
	public void test61() {
		// "cough" - "KF"
		test("cough","KF","KF");
	}

	@Test
	public void test62() {
		// "gough" - "KF"
		test("gough","KF","KF");
	}

	@Test
	public void test63() {
		// "rough" - "RF"
		test("rough","RF","RF");
	}

	@Test
	public void test64() {
		// "tough" - "TF"
		test("tough","TF","TF");
	}

	@Test
	public void test65() {
		// "cagney" - "KKN"
		test("cagney","KKN","KKN");
	}

	@Test
	public void test66() {
		// "tagliaro" - "TKLR" and "TLR"
		test("tagliaro","TKLR","TLR");
	}

	@Test
	public void test67() {
		// "biaggi" - "PJ" and "PK"
		test("biaggi","PJ","PK");
	}

	@Test
	public void test68() {
		// "san jacinto" - "SNHS"
		test("san jacinto","SNHS","SNHS");
	}

	@Test
	public void test69() {
		// Yankelovich - "ANKL"
		test("Yankelovich","ANKL","ANKL");
	}

	@Test
	public void test70() {
		// Jankelowicz - "JNKL" and "ANKL"
		test("Jankelowicz","JNKL","ANKL");
	}

	@Test
	public void test71() {
		// "bajador" - "PJTR" and "PHTR"
		test("bajador","PJTR","PHTR");
	}

	@Test
	public void test72() {
		// "cabrillo" - "KPRL" and "KPR"
		test("cabrillo","KPRL","KPR");
	}

	@Test
	public void test73() {
		// "gallegos" - "KLKS" and "KKS"
		test("gallegos","KLKS","KKS");
	}

	@Test
	public void test74() {
		// "dumb" - "TM"
		test("dumb","TM","TM");
	}

	@Test
	public void test75() {
		// "thumb" - "0M" and "TM"
		test("thumb","0M","TM");
	}

	@Test
	public void test76() {
		// "campbell" - "KMPL"
		test("campbell","KMPL","KMPL");
	}

	@Test
	public void test77() {
		// "raspberry" - "RSPR"
		test("raspberry","RSPR","RSPR");
	}

	@Test
	public void test78() {
		// "hochmeier" - "HKMR"
		test("hochmeier","HKMR","HKMR");
	}

	@Test
	public void test79() {
		// "island" - "ALNT"
		test("island","ALNT","ALNT");
	}

	@Test
	public void test80() {
		// "isle" - "AL"
		test("isle","AL","AL");
	}

	@Test
	public void test81() {
		// "carlisle" - "KRLL"
		test("carlisle","KRLL","KRLL");
	}

	@Test
	public void test82() {
		// "carlysle" - "KRLL"
		test("carlysle","KRLL","KRLL");
	}

	@Test
	public void test83() {
		// "smith" - "SM0" and "XMT"
		test("smith","SM0","XMT");
	}

	@Test
	public void test84() {
		// "schmidt" - "XMT" and "SMT"
		test("schmidt","XMT","SMT");
	}

	@Test
	public void test85() {
		// "snider" - "SNTR" and "XNTR"
		test("snider","SNTR","XNTR");
	}

	@Test
	public void test86() {
		// "schneider" - "XNTR" and "SNTR"
		test("schneider","XNTR","SNTR");
	}

	@Test
	public void test87() {
		// "school" - "SKL"
		test("school","SKL","SKL");
	}

	@Test
	public void test88() {
		// "schooner" - "SKNR"
		test("schooner","SKNR","SKNR");
	}

	@Test
	public void test89() {
		// "schermerhorn" - "XRMR" and "SKRM"
		test("schermerhorn","XRMR","SKRM");
	}

	@Test
	public void test90() {
		// "schenker" - "XNKR" and "SKNK"
		test("schenker","XNKR","SKNK");
	}

	@Test
	public void test191() {
		// "resnais" - "RSN" and "RSNS"
		test("resnais","RSN","RSNS");
	}

	@Test
	public void test92() {
		// "artois" - "ART" and "ARTS"
		test("artois","ART","ARTS");
	}

	@Test
	public void test93() {
		// "thomas" - "TMS"
		test("thomas","TMS","TMS");
	}

	@Test
	public void test94() {
		// Wasserman - "ASRM" and "FSRM"
		test("Wasserman","ASRM","FSRM");
	}

	@Test
	public void test95() {
		// Vasserman - "FSRM"
		test("Vasserman","FSRM","FSRM");
	}

	@Test
	public void test96() {
		// Uomo - "AM"
		test("Uomo","AM","AM");
	}

	@Test
	public void test97() {
		// Womo - "AM" and "FM"
		test("Womo","AM","FM");
	}

	@Test
	public void test98() {
		// Arnow -  "ARN" and "ARNF"
		test("Arnow","ARN","ARNF");
	}

	@Test
	public void test99() {
		// Arnoff - "ARNF"
		test("Arnoff","ARNF","ARNF");
	}

	@Test
	public void test100() {
		// "filipowicz" - "FLPT" and "FLPF"
		test("filipowicz","FLPT","FLPF");
	}

	@Test
	public void test101() {
		// breaux - "PR"
		test("breaux","PR","PR");
	}

	@Test
	public void test102() {
		// "zhao" - "J"
		test("zhao","J","J");
	}

	@Test
	public void test103() {
		// "thames" - "TMS"
		test("thames","TMS","TMS");
	}

	@Test
	public void test104() {
		/* ----------------------------------------------------------
		 * a little different from the "DoubleMetaphone Test" results
		 * at http://swoodbridge.com/DoubleMetaPhone/mptest.php3
		 * ----------------------------------------------------------*/

		// "jose" - "JS" and "HS" (DoubleMetaphone Test got "HS" and "")
		test("jose","JS","HS");
	}

	@Test
	public void test105() {
		// "rogier" - "RJ" and "RKR" (DoubleMetaphone Test got "RJ" and "RJR" )
		test("rogier","RJ","RKR");
	}
}
